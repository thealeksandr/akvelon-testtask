package com.akvelon.testtask.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akvelon.testtask.R;
import com.akvelon.testtask.databinding.ViewPostBinding;
import com.akvelon.testtask.models.Post;

import java.util.List;

/**
 * Created by aleksandr on 2017/12/05.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.BindingViewHolder> {

    private Context mContext;
    private List<Post> mItems;
    private OnItemClickListener mOnItemClickListener;

    public FeedAdapter(Context context) {
        mContext = context;
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewPostBinding binding
                = DataBindingUtil.inflate(
                LayoutInflater.from(mContext), R.layout.view_post, parent,
                false);
        return new BindingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingViewHolder holder, final int position) {
        ViewPostBinding binding = (ViewPostBinding) holder.binding;
        binding.setPost(mItems.get(position));
        binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onCLick(mItems.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void setItems(List<Post> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    class BindingViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        public BindingViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnItemClickListener {
        void onCLick(Post post);
    }

}
