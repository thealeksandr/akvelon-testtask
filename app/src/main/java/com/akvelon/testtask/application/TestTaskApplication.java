package com.akvelon.testtask.application;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.akvelon.testtask.database.TestTaskDatabase;

/**
 * Created by aleksandr on 2017/12/06.
 */

public class TestTaskApplication extends Application {

    private static TestTaskDatabase sDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        sDatabase = Room.databaseBuilder(this, TestTaskDatabase.class, "db").build();
    }


    public static TestTaskDatabase getDatabase() {
        return sDatabase;
    }

}
