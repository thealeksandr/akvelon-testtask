package com.akvelon.testtask.api

import com.akvelon.testtask.models.Post
import com.akvelon.testtask.models.ResultList

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by aleksandr on 2017/12/05.
 */
interface TestTaskApi {

    @GET("feed/json")
    fun feed(): Call<ResultList<Post>>
}
