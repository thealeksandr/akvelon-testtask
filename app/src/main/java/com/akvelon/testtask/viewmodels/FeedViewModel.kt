package com.akvelon.testtask.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.os.AsyncTask
import android.os.Handler
import com.akvelon.testtask.api.TestTaskApiHelper
import com.akvelon.testtask.application.TestTaskApplication

import com.akvelon.testtask.models.Post
import android.os.Looper



/**
 * Created by aleksandr on 2017/12/05.
 */

class FeedViewModel: ViewModel() {

    private var feed: LiveData<List<Post>>? = null
    val posts: LiveData<List<Post>>
        get() {
            if (feed == null) {
                feed = TestTaskApplication.getDatabase().postDao().all
            }
            return feed!!
        }

    fun updateFeed(errorListener: OnErrorListener?): LiveData<List<Post>> {
        val postsLiveData = this.posts
        AsyncTask.execute {
            val feedFromServer = TestTaskApiHelper.getFeed()
            if (feedFromServer == null) {
                val uiHandler = Handler(Looper.getMainLooper())
                uiHandler.post {
                    errorListener?.onUpdateError()
                }
                return@execute
            }
            TestTaskApplication.getDatabase().postDao().insertAll(*feedFromServer.toTypedArray())

        }
        return postsLiveData
    }

    interface OnErrorListener {
        fun onUpdateError()
    }
}
