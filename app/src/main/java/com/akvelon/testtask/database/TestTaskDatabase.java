package com.akvelon.testtask.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;



import com.akvelon.testtask.models.Post;

/**
 * Created by aleksandr on 2017/12/05.
 */
@Database(entities = {Post.class}, version = 1)
@TypeConverters(Converter.class)
public abstract class TestTaskDatabase extends RoomDatabase {

    public abstract PostDao postDao();

}
