package com.akvelon.testtask.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by aleksandr on 2017/12/06.
 */

public class Author implements Serializable {

    @SerializedName("name")
    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
