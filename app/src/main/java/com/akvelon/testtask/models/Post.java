package com.akvelon.testtask.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.akvelon.testtask.database.Converter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by aleksandr on 2017/12/05.
 */
@Entity
public class Post implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("author")
    @TypeConverters({Converter.class})
    private Author author;
    @SerializedName("content_html")
    private String content;
    @SerializedName("date_modified")
    private String dateModified;
    @SerializedName("date_published")
    private String datePublished;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String url;


    public void setId(int id) {
        this.id = id;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public Author getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public String getDateModified() {
        return dateModified;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

}
