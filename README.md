# Akvelon TestTask

News App.

To Build application from Android Studio choose in menu:      
**Build -> Rebuild Project**
         
To run application from Android Studio choose in menu:        
**Run -> Run 'App'**    
       
To buila application in command line run     
     
```
./gradlew assembleDebug
```
     
To build and install     
        
```
./gradlew installDebug
```
[Video](https://bitbucket.org/thealeksandr/akvelon-testtask/downloads/device-2017-12-11-193749.mp4)

[Design Document](https://bitbucket.org/thealeksandr/akvelon-testtask/wiki/Home)



**Other**

[Algorithm tasks](https://bitbucket.org/thealeksandr/akvelon-testtask/downloads/Algorithm%20tasks%20-%20Nikiforov%20A.O.)

[Check list](https://bitbucket.org/thealeksandr/akvelon-testtask/downloads/Check%20List%20-%20Nikiforov%20A.%20O.)